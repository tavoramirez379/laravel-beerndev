{{-- @extends('layouts.app')

@section('content')
<div class="title m-b-md">
    Beer & Dev
</div>
@if (isset($ceo))
<p>Ceo: {{ $ceo }}</p>
@else
<p>No hay ceo aún</p>
@endif
<div class="links">
    @foreach ($links as $link => $text)
        <a href="{{ $link }}">{{ $text}}</a>
    @endforeach    
</div>    
</div>
@stop
--}}
@extends('layouts.app')

@section('content')
<div class="jumbotron text-center">
    <h1>Laratter</h1>
    <nav>
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
        </ul>
    </nav>
</div>
<div class="row">
    @forelse($messages as $message)
        <div class="col-6">
            <img class="img-thumbnail" src="{{ $message['image'] }}">
            <p class="card-text">
                {{ $message['content'] }}
                <a href="/messages/{{ $message['id'] }}">Leer más</a>
            </p>
        </div>
    @empty
        <p>No hay mensajes destacados.</p>
    @endforelse
</div>
@endsection